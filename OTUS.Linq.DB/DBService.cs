﻿using OTUS.Linq.DB.Entities;
using System;
using System.Collections.Generic;

namespace OTUS.Linq.DB
{
    public class DBService
    {
        public void Initialise()
        {
            OperationUsersInitialise();
            OperationAccountsInitialise();
            OperationHistoriesInitialise();
        }

        public List<OperationHistory> OperationHistories { get; private set; }
        public List<Account> Accounts { get; private set; }
        public List<User> Users { get; private set; }

        private void OperationHistoriesInitialise()
        {
            OperationHistories = new List<OperationHistory>();
            for (int i = 0; i < 12; i++)
            {
                OperationHistories.Add(
                    new OperationHistory()
                    {
                        Id = i,
                        DateOfOperation = DateTime.Now,
                        InvoiceAmount = i * 1000,
                        TypeOfTransaction = i % 2 == 0 ? TypeOperation.TopUp : TypeOperation.Withdraw, 
                        AccountId = i % 6
                    });
            }
        }

        private void OperationAccountsInitialise()
        {
            Accounts = new List<Account>();
            for (int i = 0; i < 6; i++)
            {
                Accounts.Add(
                    new Account()
                    {
                        Id = i,
                        AccountAmount = i * 100,
                        AccountOpeningDate = DateTime.Now,
                        OwnerId = i % 3
                    });
            }
        }

        private void OperationUsersInitialise()
        {
            Users = new List<User>();
            for (int i = 0; i < 3; i++)
            {
                Users.Add(
                    new User()
                    {
                        Id = i,
                        Firstname = $"Петр {i}",
                        Surname = $"Дорохов {i}",
                        Patronymic = $"Игоревич",
                        Login = $"Петруха$$${i}",
                        Password = i.ToString(),
                        Passport = i,
                        Phone = $"{i}.{i}",
                        RegistrationDate = DateTime.Now
                    });
            }
        }
    }
}
