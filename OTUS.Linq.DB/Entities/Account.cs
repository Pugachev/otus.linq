﻿using System;

namespace OTUS.Linq.DB.Entities
{
    public class Account
    {
        public int Id { get; set; }
        public DateTime AccountOpeningDate { get; set; }
        public int AccountAmount { get; set; }
        public int OwnerId { get; set; }



        public override string ToString()
        {
            return $"Id: {Id} {Environment.NewLine}" +
                $"AccountAmount: {AccountAmount} {Environment.NewLine}" +
                $"AccountOpeningDate: {AccountOpeningDate} {Environment.NewLine}" +
                $"OwnerId: {OwnerId} {Environment.NewLine}";
        }
    }
}
