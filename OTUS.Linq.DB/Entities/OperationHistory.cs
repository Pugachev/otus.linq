﻿using System;

namespace OTUS.Linq.DB.Entities
{
    public class OperationHistory
    {
        public int Id { get; set; }
        public DateTime DateOfOperation { get; set; }
        public TypeOperation TypeOfTransaction { get; set; }
        public int InvoiceAmount { get; set; }
        public int AccountId { get; set; }

        public override string ToString()
        {
            return $"Id: {Id} {Environment.NewLine}" +
                $"DateOfOperation: {DateOfOperation} {Environment.NewLine}" +
                $"InvoiceAmount: {InvoiceAmount} {Environment.NewLine}" +
                $"TypeOfTransaction: {TypeOfTransaction} {Environment.NewLine}";
        }
    }
}
