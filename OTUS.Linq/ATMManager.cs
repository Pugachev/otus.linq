﻿using OTUS.Linq.DB;
using OTUS.Linq.DB.Entities;
using System;
using System.Linq;

namespace OTUS.Linq
{
    class ATMManager
    {
        private readonly DBService _dBService;

        public ATMManager(DBService dBService)
        {
            _dBService = dBService;
        }

        //1. Вывод информации о заданном аккаунте по логину и паролю;
        public void ShowAccountByLoginAndPassword(
            string login,
            string password)
        {
            var query = _dBService.Accounts
                 .Join(_dBService.Users,
                            a => a.OwnerId,
                            u => u.Id,
                            (accounts, user) => new
                            {
                                Account = accounts,
                                User = user
                            })
                 .Where(g => login.Equals(g.User.Login) && password.Equals(g.User.Password));

            if (!query.Any())
            {
                Console.WriteLine($"Аккаунтов по логину {login} и паролю {password} не найдено");
                return;
            }

            Console.WriteLine($"1. Вывод информации о заданном аккаунте по логину {login} и паролю {password}");
            foreach (var item in query)
            {
                Console.WriteLine(item.Account);
            }

            Console.WriteLine("--------------------------------------------------------");
        }

        //2. Вывод данных о всех счетах заданного пользователя;
        public void ShowDataAllAccountsByUser(
            User user,
            bool operationHistory = false)
        {
            var accounts = _dBService.Users
                 .GroupJoin(_dBService.Accounts,
                            u => u.Id,
                            a => a.OwnerId,
                            (user, accounts) => new
                            {
                                Accounts = accounts,
                                User = user
                            })
                 .Where(g => user.Equals(g.User))
                 .SingleOrDefault()
                 ?.Accounts;

            if (accounts == null)
            {
                Console.WriteLine($"Данных о всех счетах заданного пользователя {user} не найдено");
                return;
            }

            Console.WriteLine($"2. Вывод данных о всех счетах заданного пользователя {user}");
            foreach (var account in accounts)
            {
                Console.WriteLine(account);

                if (!operationHistory)
                    continue;

                var histories = _dBService.OperationHistories
                    .Where(h => h.AccountId == account.Id)
                    .ToList();
                if (!histories.Any())
                {
                    Console.WriteLine($"История по счету {account.Id} не найдена");
                    continue;
                }

                Console.WriteLine($"История по счету {account.Id}");
                foreach (var history in histories)
                {
                    Console.WriteLine(history);
                }
            }

            Console.WriteLine("--------------------------------------------------------");
        }

        //3. Вывод данных о всех счетах заданного пользователя, включая историю по каждому счёту;
        public void ShowDataAllAccountsByUserIncludingHistoryAccount(User user)
        { 
            Console.WriteLine($"3. Вывод данных о всех счетах заданного пользователя {user}, включая историю по каждому счёту");
            ShowDataAllAccountsByUser(user, true);
            Console.WriteLine("--------------------------------------------------------");
        }

        //4. Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта;
        public void ShowDataAllAccountsReplenishmentOperations(Account account)
        {

            var query = _dBService.OperationHistories
                .Where(h => h.AccountId == account.Id && h.TypeOfTransaction == TypeOperation.TopUp);

            var user = _dBService.Users.Where(u => u.Id == account.OwnerId).SingleOrDefault();
            foreach (var history in query)
            {
                Console.WriteLine("4. Вывод данных о всех операциях пополнения счёта");
                Console.WriteLine(history);

                Console.WriteLine("С указанием владельца каждого счёта");
                Console.WriteLine(user);
            }

            Console.WriteLine("--------------------------------------------------------");
        }

        //5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой);
        public void ShowDataAllUsersWhoseSumIsGreaterThanN(int N)
        {
            Console.WriteLine("5. Вывод данных о всех пользователях у которых на счёте сумма больше N(N задаётся из вне и может быть любой");

            var query = _dBService.Accounts
                .Where(a => a.AccountAmount > N)
                .Join(_dBService.Users,
                            a => a.OwnerId,
                            u => u.Id,
                            (account, users) => new
                            {
                                User = users
                            });

            if (!query.Any())
            {
                Console.WriteLine($"Не найдено");
                return;
            }

            foreach (var user in query)
            {
                Console.WriteLine(user.User);
            }
        }

    }
}
