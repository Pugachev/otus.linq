﻿using OTUS.Linq.DB;
using System;

namespace OTUS.Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            var dBService = new DBService();
            dBService.Initialise();
            var atm = new ATMManager(dBService);

            atm.ShowAccountByLoginAndPassword("Петруха$$$2", "2");

            atm.ShowDataAllAccountsByUser(dBService.Users[2]);

            atm.ShowDataAllAccountsByUserIncludingHistoryAccount(dBService.Users[1]);

            atm.ShowDataAllAccountsReplenishmentOperations(dBService.Accounts[4]);

            atm.ShowDataAllUsersWhoseSumIsGreaterThanN(200);

            Console.ReadLine();
        }
    }
}
